import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ImageBackground,
    Image,
    TouchableOpacity,
    TextInput,
    StyleSheet
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { getToken } from '../../utils/utils';

import { styles } from './RegisterScreen-style';

function RegisterScreen(props) {
    const [name, setName] = useState('azka');
    const [email, setEmail] = useState('azkahi@azkahi.com');
    const [password, setPassword] = useState('1234567');
    const [age, setAge] = useState('22');

    const processToken = async () => {
        const token = await getToken();

        if (token) {
            props.navigation.navigate('HomeTab');
        }
    }

    useEffect(() => {
        processToken();
    }, []);

    const submitRegister = async (val) => {
        try {
            const fetchRequest = await fetch('https://api-nodejs-todolist.herokuapp.com/user/register', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(val)
            });

            const fetchJson = await fetchRequest.json();

            if (fetchJson.user) {
                alert('User created successfully');
                
                await AsyncStorage.setItem('@token', fetchJson.token);

                props.navigation.navigate('HomeTab');
            } else {
                alert(fetchJson);
            }
        } catch (e) {
            alert(e);
        }
    }

    const handleRegister = () => {
        if (name && email && password && age) {
            submitRegister({name, email, password, age});
        }
    }

    const { navigation } = props;
    
    return(
        <View style={styles.container}>
            <View style={styles.tops}>
                <Text style={styles.text}>To-do List App</Text>
                <View style={styles.textInputContainer}>
                    <TextInput
                    style={styles.textInput}
                    placeholder="Name"
                    placeholderTextColor="white"
                    value={name}
                    onChangeText={(val) => setName(val)}
                    />
                </View>
                <View style={styles.textInputContainer}>
                    <TextInput
                    style={styles.textInput}
                    placeholder="E-mail"
                    placeholderTextColor="white"
                    value={email}
                    onChangeText={(val) => setEmail(val)}
                    />
                </View>
                <View style={styles.textInputContainer}>
                    <TextInput
                    style={styles.textInput}
                    placeholder="Password"
                    placeholderTextColor="white"
                    value={password}
                    onChangeText={(val) => setPassword(val)}
                    secureTextEntry
                    />
                </View>
                <View style={styles.textInputContainer}>
                    <TextInput
                    style={styles.textInput}
                    placeholder="Age"
                    placeholderTextColor="white"
                    value={age}
                    onChangeText={(val) => setAge(val)}
                    />
                </View>
                <TouchableOpacity onPress={() => handleRegister()} style={styles.button}>
                    <Text style={styles.text}>Daftar</Text>
                </TouchableOpacity>
                <Text style={[styles.text, {fontSize: 18, marginVertical: 10}]}>Sudah Punya Akun?</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text style={styles.text}>Masuk</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default RegisterScreen;