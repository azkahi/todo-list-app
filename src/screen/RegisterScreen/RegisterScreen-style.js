import { StyleSheet, Dimensions } from 'react-native';

const width = Dimensions.get('window').width;

export const styles = StyleSheet.create({
    container: {
        paddingTop: 30,
        flex: 1,
        justifyContent: 'center',
    },
    text: {
        fontSize: 24,
        textAlign: 'center',
        color: 'black'
    },
    image: {
        height: 150,
        width: width,
        resizeMode: 'contain',
        marginTop: 20,
        marginBottom: 40
    },
    tops: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    bottomsButton: {
        flex: 1,
        justifyContent: 'flex-end',
        flexDirection: 'column',
    },
    button: {
        marginHorizontal: 20,
        marginBottom: 20,
        marginTop: 20,
        padding: 10,
        borderRadius: 60,
        height: 60,
        width: width * 0.5,
        backgroundColor: 'orange',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInput: {
        color: 'black',
    },
    textInputContainer: {
        height: 50,
        width: width * 0.8,
        borderBottomWidth: 1,
        borderColor: 'black',
        marginVertical: 20
    }
})