import { StyleSheet, Dimensions } from 'react-native';

const width = Dimensions.get('window').width;

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerHeader: {
        paddingTop: 20,
        backgroundColor: 'tomato',
        alignItems: 'center'
    },
    containerTasks: {
        flexDirection: 'row',
        borderTopWidth: 1,
        borderColor: 'white'
    },
    bannerTask: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        padding: 10
    },
    textInput: {
        borderBottomWidth: 1,
        borderColor: 'black',
        marginHorizontal: 15
    },
    button: {
        width: 100,
        marginTop: 20,
        marginLeft: 15,
        borderWidth: 1,
        borderColor: 'tomato',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        fontSize: 20,
        color: 'tomato'
    },
    titleText: {
        fontSize: 24,
        textAlign: 'center',
        color: 'white'
    },
    tomatoText: {
        marginTop: 10,
        marginHorizontal: 15,
        fontSize: 20,
        color: 'tomato'
    },
    text: {
        fontSize: 16,
        marginHorizontal: 15,
        color: 'black'
    },
    FAB: {
        width: 60,  
        height: 60,   
        borderRadius: 30,            
        backgroundColor: 'tomato',                                    
        position: 'absolute',                                          
        bottom: 10,                                                    
        right: 10, 
        alignItems: 'center',
        justifyContent: 'center'
    },
    textFAB: {
        color: 'white'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        margin: 10
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      },
      containerTask: {
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          borderWidth: StyleSheet.hairlineWidth,
          marginVertical: 5,
          marginHorizontal: 10,
          padding: 10,
          borderRadius: 5
      }
})