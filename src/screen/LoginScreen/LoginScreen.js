import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ImageBackground,
    Image,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { getToken } from '../../utils/utils';

import { styles } from './LoginScreen-style';

function LoginScreen(props) {
    
    const [email, setEmail] = useState('azkahi@azkahi.com');
    const [password, setPassword] = useState('1234567');

    const processToken = async () => {
        const token = await getToken();

        if (token) {
            props.navigation.navigate('HomeTab');
        }
    }

    useEffect(() => {
        processToken();
    }, []);

    const storeData = async (value) => {
        try {
            await AsyncStorage.setItem('@token', value);
            alert(`Login Successful`);
            props.navigation.navigate('HomeTab');
        } catch (e) {
            alert(e);
        }
    }

    const submitLogin = async (val) => {
        const fetchRequest = await fetch('https://api-nodejs-todolist.herokuapp.com/user/login', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(val)
        });

        const fetchJson = await fetchRequest.json();

        console.log(fetchJson);

        if (fetchJson.token) {
            storeData(fetchJson.token);
        } else {
            alert(fetchJson);
        }
    }

    const handleLogin = () => {
        if (email && password) {
            submitLogin({email, password});
        }
    }

    const { navigation } = props;

    return(
        <View style={styles.container}>
            <View style={styles.tops}>
                <Text style={styles.text}>To-do List App</Text>
                <View style={styles.textInputContainer}>
                    <TextInput
                    style={styles.textInput}
                    placeholder="E-mail"
                    placeholderTextColor="white"
                    onChangeText={(value) => setEmail(value)}
                    value={email}
                    />
                </View>
                <View style={styles.textInputContainer}>
                    <TextInput
                    style={styles.textInput}
                    placeholder="Password"
                    placeholderTextColor="white"
                    secureTextEntry
                    onChangeText={(value) => setPassword(value)}
                    value={password}
                    />
                </View>
                <TouchableOpacity style={styles.button} onPress={() => handleLogin()}>
                    <Text style={styles.text}>Masuk</Text>
                </TouchableOpacity>
                <Text style={[styles.text, {fontSize: 18, marginVertical: 10}]}>Belum Punya Akun?</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                    <Text style={styles.text}>Daftar</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default LoginScreen;