import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ImageBackground,
    Image,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    Modal,
    TextInput,
    ActivityIndicator
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
const axios = require('axios');

import { getToken } from '../../utils/utils';

import { styles } from './ActiveTaskScreen-style';

function ActiveTaskScreen(props) {
    const { navigation } = props;
    const [taskData, setTaskData] = useState([]);
    const [isAdding, setIsAdding] = useState(false);
    const [loading, setLoading] = useState(true);
    const [refreshing, setRefreshing] = useState(false);
    const [taskDescription, setTaskDescription] = useState('');

    const initAllTaskData = async () => {
        const token = await getToken();
        getAllTask(token);
    }

    const getAllTask = async (token) => {
        axios({
            method: 'get',
            url: 'https://api-nodejs-todolist.herokuapp.com/task?completed=false',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                // handle success
                setTaskData(response.data.data);
                setLoading(false);
            })
            .catch((error) => {
                // handle error
                alert(error);
                setLoading(false);
            })
    }

    const postTask = async () => {
        const token = await getToken();
        setLoading(true);
        axios({
            method: 'post',
            url: 'https://api-nodejs-todolist.herokuapp.com/task',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            data: {
                description: taskDescription
            }
        })
            .then((response) => {
                // handle success
                setTaskDescription('');
                setLoading(false);
                setIsAdding(false);
                initAllTaskData();
            })
            .catch((error) => {
                // handle error
                alert(error);
                setLoading(false);
            })
    }

    const updateTask = async (id, isCompleted) => {
        setLoading(true);
        const token = await getToken();
        axios({
            method: 'put',
            url: `https://api-nodejs-todolist.herokuapp.com/task/${id}`,
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            data: {
                completed: isCompleted
            }
        })
            .then((response) => {
                // handle success
                setLoading(false);
                initAllTaskData();
            })
            .catch((error) => {
                // handle error
                alert(error);
                setLoading(false);
            })
    }

    const deleteTask = async (id) => {
        setLoading(true);
        const token = await getToken();
        axios({
            method: 'delete',
            url: `https://api-nodejs-todolist.herokuapp.com/task/${id}`,
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
        })
            .then((response) => {
                // handle success
                setLoading(false);
                initAllTaskData();
            })
            .catch((error) => {
                // handle error
                alert(error);
                setLoading(false);
            })
    }

    useEffect(() => {
        initAllTaskData();
    }, []);

    const renderItem = ({ item }) => {
        console.log(item);

        return (
            <View style={styles.containerTask}>
                { item.completed ? <Ionicons name="checkmark-circle-outline" size={20} color="tomato" /> : <Ionicons name="list-circle-outline" size={20} color="tomato" />}
                <Text numberOfLines={3}>{item.description}</Text>
                <View>
                    <TouchableOpacity
                        style={{ ...styles.openButton, backgroundColor: "tomato" }}
                        onPress={() => updateTask(item._id, !item.completed)}
                    >
                        <Text style={styles.textStyle}>Change Status</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ ...styles.openButton, backgroundColor: "tomato" }}
                        onPress={() => deleteTask(item._id)}
                    >
                        <Text style={styles.textStyle}>Delete</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }


    if (loading) {
        return(
            <View style={styles.container}>
                <ActivityIndicator size="large" color="tomato" />
            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={isAdding}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.modalText}>Type your to-do here</Text>
                            <TextInput style={styles.textInput} value={taskDescription} onChangeText={(val) => setTaskDescription(val)} />
                            <TouchableOpacity
                                style={{ ...styles.openButton, backgroundColor: "tomato" }}
                                onPress={() => postTask()}
                            >
                                <Text style={styles.textStyle}>Post</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={{ ...styles.openButton, backgroundColor: "tomato" }}
                                onPress={() => setIsAdding(false)}
                            >
                                <Text style={styles.textStyle}>Hide</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                <FlatList
                    data={taskData}
                    renderItem={renderItem}
                    keyExtractor={item => item._id}
                    refreshing={refreshing}
                    onRefresh={() => initAllTaskData()}
                />
            </View>
        );
    }
}

export default ActiveTaskScreen;