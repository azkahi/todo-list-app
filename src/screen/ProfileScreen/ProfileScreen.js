import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ImageBackground,
    Image,
    TouchableOpacity,
    StyleSheet,
    ActivityIndicator,
    TextInput
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { getToken } from '../../utils/utils';

const axios = require('axios');

import { styles } from './ProfileScreen-style';

function ProfileScreen(props) {
    const [age, setAge] = useState(0);
    const [email, setEmail] = useState('');
    const [name, setName] = useState('');
    const [allTaskDataCount, setAllTaskDataCount] = useState(0);
    const [completedTaskDataCount, setCompletedTaskDataCount] = useState(0);
    const [loading, setLoading] = useState(true);
    const [updating, setUpdating] = useState(false);

    const { navigation } = props;

    const initUserData = async () => {
        const token = await getToken();
        axios({
            method: 'get',
            url: 'https://api-nodejs-todolist.herokuapp.com/user/me',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                // handle success
                
                setAge(response.data.age);
                setEmail(response.data.email);
                setName(response.data.name);
                Promise.all([getAllTask(token), getCompletedTask(token)]).then(() => setLoading(false));
            })
            .catch((error) => {
                // handle error
                alert(error);
            })
    }

    const getAllTask = async (token) => {
        axios({
            method: 'get',
            url: 'https://api-nodejs-todolist.herokuapp.com/task',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {
            // handle success
            setAllTaskDataCount(response.data.count);
        })
        .catch((error) => {
            // handle error
            alert(error);
        })
    }

    const getCompletedTask = async (token) => {
        axios({
            method: 'get',
            url: 'https://api-nodejs-todolist.herokuapp.com/task?completed=true',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        })
        .then((response) => {
            // handle success
            setCompletedTaskDataCount(response.data.count);
        })
        .catch((error) => {
            // handle error
            alert(error);
        })
    }

    const submitEditData = async () => {
        setLoading(true);
        const token = await getToken();

        axios({
            method: 'put',
            url: 'https://api-nodejs-todolist.herokuapp.com/user/me',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            data: {
                age,
                name,
                email
            }
        })
        .then((response) => {
            // handle success
            setUpdating(false);
            setLoading(false);
        })
        .catch((error) => {
            // handle error
            alert(error);
            setLoading(false);
        })
    }

    const submitDeleteData = async () => {
        setLoading(true);
        const token = await getToken();

        axios({
            method: 'delete',
            url: 'https://api-nodejs-todolist.herokuapp.com/user/me',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
        })
        .then(async (response) => {
            // handle success
            await AsyncStorage.setItem('@token', '');
            setLoading(false);
            navigation.navigate('Login');
        })
        .catch((error) => {
            // handle error
            alert(error);
            setLoading(false);
        })
    }

    const logout = async () => {
        setLoading(true);
        
        const token = await getToken();
        
        axios({
            method: 'post',
            url: 'https://api-nodejs-todolist.herokuapp.com/user/logout',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
        })
        .then(async (response) => {
            // handle success
            await AsyncStorage.setItem('@token', '');
            setLoading(false);
            navigation.navigate('Login');
        })
        .catch((error) => {
            // handle error
            alert(error);
            setLoading(false);
        })
    }


    useEffect(() => {
        initUserData();

    }, []);

    if (loading) {
        return(
            <View style={styles.container}>
                <ActivityIndicator size="large" color="tomato" />
            </View>
        )
    } else {
        return (
            <View>
                <View style={styles.containerHeader}>
                    <Text style={styles.titleText}>View Profile</Text>
                    <Ionicons name="person-circle-outline" size={120} color="white" />
                    <View style={styles.containerTasks}>
                        <View style={styles.bannerTask}>
                            <Text style={styles.titleText}>All Task</Text>
                            <Text style={styles.titleText}>{allTaskDataCount}</Text>
                        </View>
                        <View style={styles.bannerTask}>
                            <Text style={styles.titleText}>Completed Task</Text>
                            <Text style={styles.titleText}>{completedTaskDataCount}</Text>
                        </View>
                    </View>
                </View>
                <Text style={styles.tomatoText}>Age</Text>
                { !updating ? <Text style={styles.text}>{age}</Text> : <TextInput style={styles.textInput} keyboardType="number-pad" value={age.toString()} onChangeText={(val) => setAge(val)} /> }
                <Text style={styles.tomatoText}>Email</Text>
                { !updating ? <Text style={styles.text}>{email}</Text> : <TextInput style={styles.textInput} value={email} onChangeText={(val) => setEmail(val)} /> }
                <Text style={styles.tomatoText}>Name</Text>
                { !updating ? <Text style={styles.text}>{name}</Text> : <TextInput style={styles.textInput} value={name} onChangeText={(val) => setName(val)} /> }

                { !updating ? 
                    <TouchableOpacity style={styles.button} onPress={() => setUpdating(true)}>
                        <Text style={styles.buttonText}>Edit</Text>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity style={styles.button} onPress={() => submitEditData()}>
                        <Text style={styles.buttonText}>Submit</Text>
                    </TouchableOpacity>
                }

                <TouchableOpacity style={styles.button} onPress={() => submitDeleteData()}>
                    <Text style={styles.buttonText}>Delete</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.button} onPress={() => logout()}>
                    <Text style={styles.buttonText}>Logout</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default ProfileScreen;