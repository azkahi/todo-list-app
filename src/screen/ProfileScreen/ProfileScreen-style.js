import { StyleSheet, Dimensions } from 'react-native';

const width = Dimensions.get('window').width;

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    containerHeader: {
        paddingTop: 20,
        backgroundColor: 'tomato',
        alignItems: 'center'
    },
    containerTasks: {
        flexDirection: 'row',
        borderTopWidth: 1,
        borderColor: 'white'
    },
    bannerTask: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        padding: 10
    },
    textInput: {
        borderBottomWidth: 1,
        borderColor: 'black',
        marginHorizontal: 15
    },
    button: {
        width: 100,
        marginTop: 20,
        marginLeft: 15,
        borderWidth: 1,
        borderColor: 'tomato',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    buttonText: {
        fontSize: 20,
        color: 'tomato'
    },
    titleText: {
        fontSize: 24,
        textAlign: 'center',
        color: 'white'
    },
    tomatoText: {
        marginTop: 10,
        marginHorizontal: 15,
        fontSize: 20,
        color: 'tomato'
    },
    text: {
        fontSize: 16,
        marginHorizontal: 15,
        color: 'black'
    },
})