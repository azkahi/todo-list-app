import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import AllTaskScreen from './screen/AllTaskScreen/AllTaskScreen';
import ProfileScreen from './screen/ProfileScreen/ProfileScreen';
import ActiveTaskScreen from './screen/ActiveTaskScreen/ActiveTaskScreen';
import CompletedTaskScreen from './screen/CompletedTaskScreen/CompletedTaskScreen';
import RegisterScreen from './screen/RegisterScreen/RegisterScreen';
import LoginScreen from './screen/LoginScreen/LoginScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const tabNavigatorScreenOptions = ({ route }) => ({
  tabBarIcon: ({ focused, color, size }) => {
    let iconName;

    switch (route.name) {
      case 'Profile':
        iconName = 'person-outline';
        break;
      case 'All Task':
        iconName = 'list-outline';
        break;
      case 'Active Task':
        iconName = 'alarm-outline';
        break;
      case 'Completed Task':
        iconName = 'checkmark-circle-outline';
        break;
      default:
        iconName = 'person-outline';
        break;
    }
    if (route.name === 'Home') {
      iconName = 'home-outline';
    } else if (route.name === 'Profile') {
    }

    // You can return any component that you like here!
    return <Ionicons name={iconName} size={size} color={color} />;
  },
});

function HomeTab() {
  return (
    <Tab.Navigator
      screenOptions={tabNavigatorScreenOptions}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}
    >
      <Tab.Screen name="All Task" component={AllTaskScreen} />
      <Tab.Screen name="Active Task" component={ActiveTaskScreen} />
      <Tab.Screen name="Completed Task" component={CompletedTaskScreen} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }} >
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
        <Stack.Screen name="HomeTab" component={HomeTab} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;